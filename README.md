#** NexusUp v1.0.0.4 **#
##by SuperR##

##**FEATURES**##

**Download newest firmware for your Nexus device**  
**Extract all img files**  
**Flash images using fastboot (included)**
**Option to skip userdata.img and recovery.img**

##**USAGE:**##

1. Run "nexusup" from it's location in terminal  
2. Allow NexusUp to download the newest firmware for your device.  
3. Enjoy!  

**OR**

1. Copy official Nexus .tgz/.zip package to the workdir.  
2. Run "nexusup" from it's location in terminal  
3. Enjoy!  

##**EXAMPLE:**##

In your terminal, type the following where "/home/user/location/" is the directory where the script lives:

```
cd /home/user/location/
./nexusup
```

**OR**

Double-click the nexusup file and choose "Run in Terminal" if your OS supports it.

##**DEPENDENCIES:**##

* tar
* wget
* grep
* gawk
* rm
* dirname
* basename
* sudo
* mkdir
* ls
